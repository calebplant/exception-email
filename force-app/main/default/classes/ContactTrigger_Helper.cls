public with sharing class ContactTrigger_Helper {
    public static void doBeforeInsert(List<Contact> newCons)
    {
        for(Contact eachCon : newCons) {
            try {
                if(eachCon.FirstName == 'throwAnError') {
                    System.debug('throwing error');
                    MyTesterException ex = new MyTesterException('First name cannot be \"throwAnError\"!');
                    throw ex;
                }
            } catch (MyTesterException e) {
                System.debug('Handling MyTesterException');
                EventBus.publish(new Send_Admin_Email__e(
                                            Time__c=System.now(),
                                            Type__c=e.getTypeName(),
                                            Message__c=e.getMessage(),
                                            User__c=UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ' (' + UserInfo.getUserId() +')'));
                eachCon.addError(e);
            }
        }
    }
}
