public with sharing class AdminEmailService {

    private static final String adminAddressDisplayName = 'System Admin';
    private static final String systemAddressCacheKeyName = 'System';

    private List<Messaging.SingleEmailMessage> emailsToSend;

    public static void sendExceptionMessage(Send_Admin_Email__e ev)
    {
        System.debug('START sendExceptionMessage');
        List<User> adminUsers = [SELECT Id, FirstName FROM User
                                WHERE Profile.PermissionsCustomizeApplication = True 
                                AND isActive = True];
        OrgWideEmailAddress sender = getSystemSender();

        List<Messaging.SingleEmailMessage> outboundEmails = new List<Messaging.SingleEmailMessage>();
        for(User eachAdminUser : adminUsers) {
            Messaging.SingleEmailMessage outboundEmail = new Messaging.SingleEmailMessage();
            outboundEmail.setTargetObjectId(eachAdminUser.Id);
            outboundEmail.setTreatTargetObjectAsRecipient(true);
            outboundEmail.setSaveAsActivity(false);
            outboundEmail.setUseSignature(false);
            outboundEmail.setSubject(AdminEmailUtils.buildSubject('Unspecified'));
            outboundEmail.setHtmlBody(AdminEmailUtils.buildHtmlBody(ev, eachAdminUser));
            if(sender != null) {
                outboundEmail.setOrgWideEmailAddressId(sender.Id);
            }
            outboundEmails.add(outboundEmail);
        }
        System.debug('Sending emails:');
        for(Messaging.SingleEmailMessage eachEmail : outboundEmails) {
            System.debug(eachEmail);
        }
        Messaging.sendEmail(outboundEmails);
    }

    private static OrgWideEmailAddress getSystemSender()
    {
        OrgWideEmailAddress sender = AdminEmailUtils.checkCacheForEmailAddress(systemAddressCacheKeyName);
        if(sender == null) {
            sender = [SELECT Id FROM OrgWideEmailAddress
                     WHERE DisplayName = :adminAddressDisplayName LIMIT 1];
                     AdminEmailUtils.setCacheForEmailAddress(systemAddressCacheKeyName, sender.Id);
        }
        return sender;
    }
}
