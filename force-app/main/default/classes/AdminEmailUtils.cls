public with sharing class AdminEmailUtils {
    public static OrgWideEmailAddress checkCacheForEmailAddress(String key)
    {
        Cache.OrgPartition orgPart = Cache.Org.getPartition('local.EmailAddressCache');
        return (OrgWideEmailAddress)orgPart.get(key);
    }

    public static void setCacheForEmailAddress(String key, String value)
    {
        Cache.OrgPartition orgPart = Cache.Org.getPartition('local.EmailAddressCache');
        orgPart.put(key, value, 300);
    }

    public static String buildSubject(String subject)
    {
        String result = 'Exception Occurred: ' + subject;
        return result; 
    }

    public static String buildHtmlBody(Exception ex, User adminUser)
    {
        String result = 'Dear ' + adminUser.FirstName + ',<br><br>' +
        'The following exception occurred during execution: <br><br>' +
        '<b>Time: </b>' + System.now() + '<br>' +
        '<b>User: </b>' + UserInfo.getFirstName() + ' ' + UserInfo.getLastName() + ' (' + UserInfo.getUserId() +')<br>' +
        '<b>Type: </b>' + ex.getTypeName() + '<br>' +
        '<b>Message: </b>' + ex.getMessage() + '<br>' +
        '<b>Trace: </b>' + ex.getStackTraceString();
        
        return result;
    }   

    public static String buildHtmlBody(Send_Admin_Email__e ev, User adminUser)
    {
        String result = 'Dear ' + adminUser.FirstName + ',<br><br>' +
        'The following exception occurred during execution: <br><br>' +
        '<b>Time: </b>' + ev.Time__c + '<br>' +
        '<b>User: </b>' + ev.User__c + '<br>' +
        '<b>Type: </b>' + ev.Type__c + '<br>' +
        '<b>Message: </b>' + ev.Message__c + '<br>';

        return result;
    }   
}
