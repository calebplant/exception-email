trigger SendAdminEmail_Trigger on Send_Admin_Email__e (after insert) {
    insert new Contact(LastName='biff');
    if(Trigger.isAfter && Trigger.isInsert) {
        for(Send_Admin_Email__e eachEvent : Trigger.New) {
            System.debug('Send_Admin_Email__e after insert');
            AdminEmailService.sendExceptionMessage(eachEvent);
        }
    }
}