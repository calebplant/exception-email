trigger ContactTrigger on Contact (before insert) {
    if(Trigger.isBefore && Trigger.isInsert) {
        System.debug('START ContactTrigger_Helper :: doBeforeInsert');
        ContactTrigger_Helper.doBeforeInsert(Trigger.new);
        System.debug('FINISH ContactTrigger_Helper :: doBeforeInsert');
    }
}